package main

import (
	"OS_Practice3/cmd"
	"log"
	"os"
)

func main() {
	var err error
	//TODO: Add help message
	if len(os.Args) < 1 {
		log.Fatal("Please set comands bruteforce/auth")
	}

	command := os.Args[1]
	//TODO: Think about method for chose program
	for _, cmd := range cmd.Cmds {
		if cmd.Name() == command {
			if err = cmd.Init(os.Args[2:]); err != nil {
				log.Fatal(err)
			}

			if err = cmd.Run(); err != nil {
				log.Fatal(err)
			}
		}
	}
}
