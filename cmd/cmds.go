package cmd

type cmds interface {
	Name() string
	Init(args []string) error
	Run() error
}

var Cmds []cmds = []cmds{
	NewBruteCmd(),
	NewAuthCmd(),
}
