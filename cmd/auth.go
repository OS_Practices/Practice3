package cmd

import (
	"flag"
	"fmt"
)

type AuthCmd struct {
	fs   *flag.FlagSet
	user string
	pass string
}

func NewAuthCmd() *AuthCmd {
	authCmd := &AuthCmd{
		fs: flag.NewFlagSet("auth", flag.ContinueOnError),
	}

	authCmd.fs.StringVar(&authCmd.pass, "pass", "pass", "This flag set password for auth. Default: 'pass'.")
	authCmd.fs.StringVar(&authCmd.user, "user", "user", "This flag set user for auth. Default: 'user'.")

	authCmd.fs.Usage = func() {
		fmt.Println("This program start server on localhost:7777 and wait get queryes.")
		authCmd.fs.PrintDefaults()
	}

	return authCmd
}

func (auth *AuthCmd) Name() string {
	return auth.fs.Name()
}

func (auth *AuthCmd) Init(args []string) error {

	return auth.fs.Parse(args)
}

// TODO: Add body for Run func
func (auth *AuthCmd) Run() error {
	fmt.Println("Yahoo! This auth server!!!")
	return nil
}
