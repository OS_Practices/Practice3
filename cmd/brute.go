package cmd

import (
	"OS_Practice3/brute"
	"flag"
	"fmt"
)

type BruteCmd struct {
	fs   *flag.FlagSet
	user string
	url  string
}

func NewBruteCmd() *BruteCmd {
	bCmd := &BruteCmd{
		fs:  flag.NewFlagSet("bruteforce", flag.ContinueOnError),
		url: "none",
	}

	bCmd.fs.StringVar(&bCmd.user, "user", "admin", "Flag for set user for bruteforce password. \n\tDefault user: 'admin'")

	bCmd.fs.Usage = func() {
		fmt.Println("This program bruteforce password for user.")
		fmt.Println("Syntax: bruteforce -user [user] [url]")
		bCmd.fs.PrintDefaults()
	}

	return bCmd
}

func (bruteCmd *BruteCmd) Name() string {
	return bruteCmd.fs.Name()
}

func (bruteCmd *BruteCmd) Init(args []string) error {
	if err := bruteCmd.fs.Parse(args); err != nil {
		return err
	}

	bruteCmd.url = bruteCmd.fs.Arg(0)

	return nil
}

// TODO: Create body run func
func (bruteCmd *BruteCmd) Run() error {
	brute, err := brute.NewBrute(bruteCmd.url, bruteCmd.user)

	if err != nil {
		return err
	}

	brute.StartBruteForce()

	return nil
}
